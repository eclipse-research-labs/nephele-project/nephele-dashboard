.PHONY: all push push-frontend push-backend frontend backend

all: frontend backend

frontend:
	docker build -t nepheleproject/frontend ./frontend

backend:
	docker build -t nepheleproject/backend ./backend

push: push-frontend push-backend

push-frontend:
	docker push nepheleproject/frontend

push-backend:
	docker push nepheleproject/backend
