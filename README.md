# Nephele-Dashboard
This repository hosts the Nephele Dashboard implementation consisting of a Flask REST API that serves as the back-end of the Dashboard and a front-end.

## Docker images
To build the container images for the frontend and the backend simply run:
```bash
make all
```
If you want to push the images and you are authorized to Dockerhub run:
```bash
make push
```

## Kubernetes deployment guide
### Setup the Nephele platform components
1. Install `traefik` by running :
    ```bash
    $ helm install traefik traefik/traefik \
        -n traefik --create-namespace \
        --values traefik-values.yml
    ```
    with the values file:
    ```yaml
    service:
    enabled: true
    annotations: {}
    type: LoadBalancer
    externalIPs:
        - X.X.X.X
    ```
    where `X.X.X.X` is a public IP that Traefik will asign to LoadBalancer services.

2. Install `cert-manager` manually (according to the cert-manager documentation, it should not be installed as a Helm sub-chart):
    ```bash
    $ kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.3/cert-manager.yaml
    ```

3. Create a Persistent Volume and a Persistent Volume Claim by running `kubectl apply -f pv.yaml` where `pv.yaml` contains a PersistentVolume like:
    ```yaml
    kind: PersistentVolume
    apiVersion: v1
    metadata:
    name: postgres-pv
    namespace: nephele-platform
    labels:
        type: local
        app: postgres
    spec:
    storageClassName: "standard"
    capacity:
        storage: 1Gi
    accessModes:
        - ReadWriteMany
    persistentVolumeReclaimPolicy: Retain
    hostPath:
        path: "/mnt/data"
    ---
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
    name: postgres-pvc
    namespace: nephele-platform
    labels:
        app: postgres
    spec:
    storageClassName: "standard"
    accessModes:
        - ReadWriteMany
    resources:
        requests:
        storage: 1Gi
    ```

4. Create three secrets with the backend, Postgres and Keycloak credentials. Run `kubectl apply -f secrets.yaml` where `secrets.yaml` is a file:
    ```yaml
    apiVersion: v1
    kind: Secret
    metadata:
        name: postgres-credentials
        namespace: nephele-platform
    type: Opaque
    stringData:
        POSTGRES_USER: XXXXXXXXX
        POSTGRES_PASSWORD: XXXXXXXXX
        POSTGRES_DB: XXXXXXXXX
    ---
    apiVersion: v1
    kind: Secret
    metadata:
        name: keycloak-secrets
        namespace: nephele-platform
    type: Opaque
    stringData:
        KEYCLOAK_ADMIN: XXXXXXXXX
        KEYCLOAK_ADMIN_PASSWORD: XXXXXXXXX
    ---
    apiVersion: v1
    kind: Secret
    metadata:
        name: nephele-backend
        namespace: nephele-platform
    type: Opaque
    stringData:
        FLASK_USERNAME: XXXXXXXXX
        FLASK_PASSWORD: XXXXXXXXX
        CLIENT_ID: XXXXXXXXX
        CLIENT_SECRET: XXXXXXXXX
        ADMIN_DASHBOARD_PASSWORD: XXXXXXXXX
    ```

5. Install the helm chart:
    ```bash
    helmchart$ helm install platform . --namespace nephele-platform --create-namespace
    ```

6. Upload the Keycloak realm json file.

## Backend
### Run docker compose that runs keycloak, postgresql, flask_backend, volume and network
There is a docker compose file that runs the backend in the `archive` directory if the appropriate `.env` file is present:
```bash
$ docker-compose up -d
```

## Frontend
- Install the dependencies
    ```bash
    frontend$ yarn
    ```
- Start the app in development mode (hot-code reloading, error reporting, etc.)
    ```bash
    frontend$ quasar dev
    ```
- Lint the files
    ```bash
    frontend$ yarn lint
    ```
- Format the files
    ```bash
    frontend$ yarn format
    ```
- Build the app for production
    ```bash
    frontend$ quasar build
    ```
- Serve the build
    ```bash
    frontend$ quasar serve dist/spa --history
    ```
- Customize the configuration
    See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).


## NEPHELE Dashboard Functionalities
The Dashboard is currently under development using Vue.js as frontend and Python Flask as backend.

- User Management
- Create Projects for each User
- The developer can create Hyper Distributed Application Graphs by combining various application components that use already created artifacts hosted on a Hyper Distributed Application Registry.
- The developer can edit the application graph descriptor
- Keycloak is used for Authorization and Authentication to manage the user profiles and the roles of  the users and to coordinate the access to the respective repositories and artifacts of the Development Environment.
- The developer can request the deployment of the created Graph through the dashboard
- Grafana dashboard are also utilized to depict metrics and real-time information both for the application graph of the users and for the NEPHELE infrastructure.
- The developer can monitoring performance metrics for the deployed applications from Grafana
- An Nginx reverse proxy instance is used to forward the client traffic to the Dashboard  from the public internet
- The deployment of the Dashboard along with all respective technologies used are coordinated by the CI/CD pipeline developed in T5.3.
A video showing a navigation to the Dashboard can be found [here](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/nephele-videos/-/blob/main/nephele-platform-demo1-HDAG-NTUA.mp4)