import axios from 'axios';
import {
  AxiosInstance,
  AxiosResponse,
  AxiosError,
  AxiosRequestConfig,
  InternalAxiosRequestConfig,
} from 'axios';
import { useStore } from 'vuex';
import qs from 'qs';
import { useRouter } from 'vue-router';

const router = useRouter();

const api: AxiosInstance = axios.create({
  baseURL: process.env.VUE_APP_BASE_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

api.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    const token: string | null = localStorage.getItem('accessToken');

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  },
  (error: Error) => {
    return Promise.reject(error);
  },
);

api.interceptors.response.use(
  (response: AxiosResponse) => {
    return response.data;
  },
  async (error: AxiosError) => {
    const originalRequest = error.config as AxiosRequestConfig;

    if (error.response?.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;

      try {
        const newToken: string = await renewToken(); // Attempt to renew the token

        if (!newToken) {
          return Promise.reject(error);
        }

        originalRequest.headers.Authorization = `Bearer ${newToken}`;
        return api(originalRequest);
      } catch (renewError) {
        console.error('Error renewing token: ', renewError);
        return Promise.reject(renewError);
      }

      return Promise.reject(error);
    }
    if (error.response?.status === 403) {
      console.error('Forbidden: ', error.response.data.message);
      localStorage.setItem('accessToken', '');
      localStorage.setItem('refreshToken', '');
      router.push('/login');
      return Promise.reject(error);
    }
  },
);

async function renewToken(): Promise<string> {
  const store = useStore();

  try {
    const refresh: string | null = localStorage.getItem('refreshToken');

    const response = await api.post(
      '/refresh_token',
      qs.stringify({ refresh_token: refresh }),
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      },
    );

    localStorage.setItem('accessToken', response.access_token);
    store.commit('setTokens', {
      accessToken: response.token,
      refreshToken: refresh,
    });

    return response.token;
  } catch (error) {
    let message: string = 'Unknown error';

    if (error instanceof Error) message = error.message;
    else message = String(error);

    console.error('Error renewing token: ', message);
    localStorage.setItem('accessToken', '');
    localStorage.setItem('refreshToken', '');

    return '';
  }
}

export { api };
