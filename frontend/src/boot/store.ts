import { boot } from 'quasar/wrappers';
import store from '../appCtx/index';

export default boot(({ app }) => {
  app.use(store);
});
