import { RouteRecordRaw } from 'vue-router';
// import { h } from 'vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/IndexPage.vue'),
        meta: { breadcrumb: 'Home' },
      },
      {
        path: '/login',
        component: () => import('pages/LoginPage.vue'),
        meta: { hideBreadcrumbs: true },
      },
      {
        path: '/delete_account',
        component: () => import('pages/DeleteAccountPage.vue'),
        meta: { breadcrumb: "Delete Account" },
      },
      {
        path: '/delete_project',
        component: () => import('pages/DeleteProjectPage.vue'),
        meta: { breadcrumb: "Delete Project" },
      },
      {
        path: '/register',
        component: () => import('pages/RegisterPage.vue'),
        meta: { hideBreadcrumbs: true },
      },
      {
        path: '/change_password',
        component: () => import('pages/ChangePasswordPage.vue'),
        meta: { BreadCrumb: 'Change Password' },
      },
      {
        path: '/manage_project',
        component: () => import('pages/ManageProjectMembersPage.vue'),
        meta: { BreadCrumb: 'Manage Project Members ' },
      },
      {
        path: '/create_project',
        component: () => import('pages/CreateProjectPage.vue'),
        meta: { breadcrumb: 'Create Project' },
      },
      {
        path: '/create_project_admin',
        component: () => import('pages/CreateProjectAdminPage.vue'),
        meta: { breadcrumb: 'Create Project Admin' },
      },
      {
        path: '/operations',
        component: () => import('pages/OperationsPage.vue'),
        meta: { breadcrumb: 'Operations' },
      },
      {
        path: '/development_environment',
        component: () => import('pages/DevelopmentEnvironment.vue'),
        meta: { breadcrumb: 'Development Environment' },
      },
      {
        path: '/infrastructure',
        component: () => import('pages/InfrastructurePage.vue'),
        meta: { breadcrumb: 'Infastructure' },
      },
      {
        path: '/software_artifacts',
        component: () => import('pages/SoftwareArtifactsPage.vue'),
        children: [
          {
            path: 'virtual_objects',
            component: () => import('pages/VirtualObjects.vue'),
            meta: { breadcrumb: 'Virtual Objects' },
          },
          {
            path: 'composite_virtual_objects',
            component: () => import('pages/CompositeVirtualObjects.vue'),
            meta: { breadcrumb: 'Composite Virtual Objects' },
          },
          {
            path: 'application_component',
            component: () => import('pages/ApplicationComponent.vue'),
            meta: { breadcrumb: 'Application Component' },
          },
          {
            path: 'application_graph',
            component: () => import('pages/ApplicationGraph.vue'),
            meta: { breadcrumb: 'Application Graph' },
            children: [
              {
                path: ':name',
                component: () =>
                  import('pages/ApplicationNetworkGraphPage.vue'),
                props: true,
                meta: {
                  breadcrumb: 'Application Network Graph',
                  isChild: true,
                },
              },
            ],
          },
        ],
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
