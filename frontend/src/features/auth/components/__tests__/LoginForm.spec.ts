import { VueWrapper, mount } from '@vue/test-utils';
import { expect, describe, it, vi } from 'vitest';
import { Quasar } from 'quasar';

import LoginForm from '../LoginForm.vue';

const i18n = {
  t: (key: string) => key,
};

vi.mock('vue-i18n', () => ({
  useI18n: () => i18n,
}));

const wrapper: VueWrapper = mount(LoginForm, {
  global: {
    plugins: [Quasar],
  },
});

describe('LoginForm', () => {
  it('should render the login form', () => {
    expect(wrapper.find('form')).toBeTruthy();
  });

  it('should have two input fields', () => {
    expect(wrapper.findAll('input')).toHaveLength(2);
  });

  it('should have one submit button', () => {
    expect(wrapper.find('button[type="submit"]')).toBeTruthy();
  });
});
