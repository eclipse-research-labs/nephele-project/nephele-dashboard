import { api } from '../../../utils/http-client';
import { ref } from 'vue';
import { useStore } from 'vuex';

interface AuthData {
  email: string;
  password: string;
  username: string;
}

interface DeleteAccountData {
  password: string;
  username: string;
}

interface AuthResponse {
  accessToken: string;
  refreshToken: string;
}

interface RegisterData {
  email: string;
  password: string;
  username: string;
  admin_dashboard_password: string,

}

interface ChangePasswordrData {
  new_password: string;
  password: string;
  username: string;
}


export function useAuth() {
  const isLoading = ref(false);
  const error = ref<string | null>(null);
  const store = useStore();

  const login = async (data: AuthData) => {
    isLoading.value = true;
    error.value = null;

    try {
      const response: AuthResponse = await api.post('/login', data);
      store.commit('setTokens', {
        accessToken: response.access_token,
        refreshToken: response.refresh_token,
      });

      localStorage.setItem('accessToken', response.access_token);
      localStorage.setItem('refreshToken', response.refresh_token);
      return {
        accessToken: response.accessToken,
        refreshToken: response.refreshToken,
      };
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  const register = async (data: RegisterData) => {
    isLoading.value = true;
    error.value = null;

    try {
      await api.post('/register', data);
      return;
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  const deleteAccount = async (data: DeleteAccountData) => {
    isLoading.value = true;
    error.value = null;

    try {
      await api.post('/delete_account', data);
      return;
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  const changePassword = async (data: ChangePasswordrData) => {
    isLoading.value = true;
    error.value = null;

    try {
      await api.post('/change_password', data);
      return;
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  return {
    isLoading,
    error,
    register,
    login,
    changePassword,
    deleteAccount
  };
}
