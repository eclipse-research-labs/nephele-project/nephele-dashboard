export default {
  // Auth translations

  // Input fields
  emailInputLabel: 'Email',
  usernameInputLabel: 'Username',
  passwordInputLabel: 'Password',
  projectInputLabel: 'The name of your project',
  oldPasswordInputLabel: 'Old password',
  newPasswordInputLabel: 'New password',
  adminDashboardPasswordLabel: 'Admin password',
  // Input rules
  emailInputEmptyRule: 'Please enter your email',
  passwordInputEmptyRule: 'Please enter your password',
  usernameInputEmptyRule: 'Please enter your username',
  projectInputEmptyRule: 'Please enter the nam eof your project',
  // Login variables
  loginHeader: 'Hello again!',
  loginSubtitle: 'Please login to your account',
  signInLabel: 'Sign in',
  forgotPasswordLabel: 'Forgot password?',
  noAccountLabel: "Don't have an account?",
  // Register variables
  registerHeader: "Create a new user",
  registerSubtitle: "Add the user's credentials",
  signUpLabel: 'Sign up',
  alreadyAccountLabel: 'Already have an account?',
  // Change password variables
  changePassHeader: "Change your password",
  changePassSubtitle: 'Add your new credentials',
  changePassLabel: 'Save',
};
