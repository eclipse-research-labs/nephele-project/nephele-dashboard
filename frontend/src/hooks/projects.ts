import { ref } from 'vue';
import { useStore } from 'vuex';
import qs from 'qs';
import { api } from '../utils/http-client';

interface ManageProjectData {
  project: string;
  username: string;
  admin_dashboard_password: string,

}
interface DeleteProjectData {
  project: string;
  admin_dashboard_password: string,

}

export function useProjects() {
  const projects = ref({ groups: [] });
  const isLoading = ref(false);
  const error = ref(null);
  const store = useStore();

  const fetchProjects = async () => {
    isLoading.value = true;
    error.value = null;
    try {
      const response = await api.post('/user_groups');
      projects.value = response;

      // Automatically set the first project if none is set and projects are available
      if (
        !store.state.projectName &&
        projects.value.groups &&
        projects.value.groups.length > 0
      ) {
        store.commit('setProjectName', projects.value.groups[0].name);
      }
    } catch (e) {
      error.value = e.message;
    } finally {
      isLoading.value = false;
    }
  };

  const createProject = async (projectName: string) => {
    isLoading.value = true;
    error.value = null;
    try {
      await api.post(
        '/create_project',
        qs.stringify({ project: projectName }),
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      );
      await fetchProjects(); // Refresh the list of projects
    } catch (e) {
      error.value = e.message;
    } finally {
      isLoading.value = false;
    }
  };


  const createProjectAdmin = async (projectName: string, admin_dashboard_password:string) => {
    isLoading.value = true;
    error.value = null;
    try {
      await api.post('/create_project_no_assigned_users', { project: projectName, admin_dashboard_password: admin_dashboard_password });
      return;
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }

  };


  const addUserToProject = async (data: ManageProjectData) => {
      isLoading.value = true;
      error.value = null;
  
      try {
        await api.post('/add_user_to_project', data);
        return;
      } catch (err) {
        error.value = err.response?.data?.message || 'An unknown error occurred';
      } finally {
        isLoading.value = false;
      }
    };


    const removeUserFromProject = async (data: ManageProjectData) => {
      isLoading.value = true;
      error.value = null;
  
      try {
        await api.post('/remove_user_from_project', data);
        return;
      } catch (err) {
        error.value = err.response?.data?.message || 'An unknown error occurred';
      } finally {
        isLoading.value = false;
      }
    };

    const deleteProject = async (data: DeleteProjectData) => {
      isLoading.value = true;
      error.value = null;
  
      try {
        await api.post('/delete_project', data);
        return;
      } catch (err) {
        error.value = err.response?.data?.message || 'An unknown error occurred';
      } finally {
        isLoading.value = false;
      }
    };

  return { projects, isLoading, error, fetchProjects, createProject,createProjectAdmin,addUserToProject,removeUserFromProject, deleteProject };
}
