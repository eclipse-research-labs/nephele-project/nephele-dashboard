import { ref } from 'vue';
import { useStore } from 'vuex';
import { api } from '../utils/http-client';

export function useFetchOperations() {
  const operations = ref([]);
  const isLoading = ref(false);
  const error = ref(null);
  const store = useStore();

  const fetchOperations = async () => {
    isLoading.value = true;
    error.value = null;

    try {
      const projectName = store.getters.projectName;
      const response = await api.post('/app_graphs', {
        project_name: projectName,
      });

      operations.value = Object.values(response);
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  const startOperation = async (name: string) => {
    isLoading.value = true;
    error.value = null;

    try {
      //   const projectName = store.getters.projectName;
      const response = await api.post('/start_graph', {
        graph_name: name,
      });

      operations.value = Object.values(response);
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  const stopOperation = async (name: string) => {
    isLoading.value = true;
    error.value = null;

    try {
      const response = await api.post('/stop_graph', {
        graph_name: name,
      });
      // await api.delete('/graph/' + name);
      operations.value = Object.values(response);
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  return {
    isLoading,
    error,
    operations,
    fetchOperations,
    startOperation,
    stopOperation,
  };
}
