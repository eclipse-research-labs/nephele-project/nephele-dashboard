import { ref } from 'vue';
import { useStore } from 'vuex';
import { api } from '../utils/http-client';

export function useFetchDescriptors() {
  const descriptor = ref([]);

  const isLoading = ref(false);
  const error = ref(null);
  const store = useStore();

  const fetchDescriptor = async (id: string) => {
    isLoading.value = true;
    error.value = null;

    try {
      const projectName = store.getters.projectName;
      const response = await api.post('/fetch_artifacts_information', {
        artifact_id: id,
        project_name: projectName,
      });

      descriptor.value = Object.values(response);
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  const publishHdag = async (content: string) => {
    isLoading.value = true;
    error.value = null;

    try {
      const projectName = store.getters.projectName;
      await api.post('/publish_hdag', {
        hdag_descriptor: content,
        project_name: projectName,
      });
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  return {
    isLoading,
    error,
    descriptor,
    fetchDescriptor,
    publishHdag,
  };
}
