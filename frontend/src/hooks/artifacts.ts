import { ref } from 'vue';
import { useStore } from 'vuex';
import { api } from '../utils/http-client';

export function useFetchArtifacts() {
  const artifacts = ref([]);
  const isLoading = ref(false);
  const error = ref(null);
  const store = useStore();

  const fetchArtifacts = async (type: string) => {
    isLoading.value = true;
    error.value = null;

    try {
      const projectName = store.getters.projectName;
      const response = await api.post('/fetch_artifacts', {
        artifact_type: type,
        project_name: projectName,
      });

      artifacts.value = Object.values(response);
      console.log(artifacts.value);
      return artifacts.value;
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  return {
    isLoading,
    error,
    artifacts,
    fetchArtifacts,
  };
}
