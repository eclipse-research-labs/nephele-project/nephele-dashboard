import { ref } from 'vue';
import { api } from '../utils/http-client';

export function useFetchClusters() {
  const clusters = ref({});

  const isLoading = ref(false);
  const error = ref(null);

  const fetchClusters = async () => {
    isLoading.value = true;
    error.value = null;

    try {
      const response = await api.get('/clusters');

      clusters.value = Object.values(response);
      console.log(clusters.value, 'clusters');
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  return {
    isLoading,
    error,
    clusters,
    fetchClusters,
  };
}
