import { ref } from 'vue';
import { api } from '../utils/http-client';

export function useFetchDevices() {
  const devices = ref({});

  const isLoading = ref(false);
  const error = ref(null);

  const fetchDevices = async () => {
    isLoading.value = true;
    error.value = null;

    try {
      const response = await api.get('/devices');

      devices.value = Object.values(response);
    } catch (err) {
      error.value = err.response?.data?.message || 'An unknown error occurred';
    } finally {
      isLoading.value = false;
    }
  };

  return {
    isLoading,
    error,
    devices,
    fetchDevices,
  };
}
