import { createStore } from 'vuex';

interface User {
  [key: string]: unknown;
}

interface UserPreferences {
  [key: string]: unknown;
}

const store = createStore({
  state: {
    user: JSON.parse(localStorage.getItem('user') || 'null') as User | null,
    counter: 0,
    projectName: localStorage.getItem('projectName') || '',
    accessToken: localStorage.getItem('accessToken') || '',
    refreshToken: localStorage.getItem('refreshToken') || '',
    userPreferences: JSON.parse(
      localStorage.getItem('preferences') || '{}',
    ) as UserPreferences,
  },
  mutations: {
    increment(state) {
      state.counter++;
    },
    setProjectName(state, projectName) {
      state.projectName = projectName;
      localStorage.setItem('projectName', projectName);
    },
    setUser(state, user: User | null) {
      state.user = user;
    },
    setUserPreference(state, { key, value }: { key: string; value: unknown }) {
      state.userPreferences[key] = value;
    },
    setTokens(state, { accessToken, refreshToken }) {
      state.accessToken = accessToken;
      state.refreshToken = refreshToken;
      localStorage.setItem('accessToken', accessToken);
      localStorage.setItem('refreshToken', refreshToken);
    },
    clearTokens(state) {
      state.accessToken = '';
      state.refreshToken = '';
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
      localStorage.removeItem('projectName');
    },
    logout(state) {
      state.user = null;
      state.accessToken = '';
      state.refreshToken = '';
      localStorage.removeItem('user');
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
    },
  },
  actions: {
    increment({ commit }) {
      commit('increment');
    },
    setUser({ commit }, user) {
      commit('setUser', user);
    },
    setUserPreference({ commit }, payload) {
      commit('setUserPreference', payload);
    },
    updateProjectName({ commit }, projectName) {
      console.log('updateProjectName', projectName);
      commit('setProjectName', projectName);
    },
  },
  getters: {
    user: (state) => state.user,
    counter: (state) => state.counter,
    accessToken: (state) => state.accessToken,
    refreshToken: (state) => state.refreshToken,
    getUserPreference: (state) => (key: string) => state.userPreferences[key],
    projectName: (state) => state.projectName,
  },
  plugins: [
    (store) => {
      // called when the store is initialized
      store.subscribe((mutation, state) => {
        // Store the state in localStorage whenever it changes
        if (mutation.type === 'setUser') {
          localStorage.setItem('user', JSON.stringify(state.user));
        } else if (mutation.type === 'setUserPreference') {
          localStorage.setItem(
            'preferences',
            JSON.stringify(state.userPreferences),
          );
        }
      });
    },
  ],
});

export default store;
