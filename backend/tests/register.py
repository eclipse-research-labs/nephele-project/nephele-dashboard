import requests

# Registration endpoint URL
registration_url = 'http://localhost:5000/register'

# Request payload
data = {
    "username": "example_user",
    "email": "example@example.com",
    "password": "your_password"
}

# Make the request
response = requests.post(registration_url, json=data)

# Check if the request was successful
if response.status_code == 200:
    # Extract the access token from the JSON response
    access_token = response.json().get('access_token')
    print ("User is Registered")
    print("Access token:", access_token)
else:
    print('Registration failed')