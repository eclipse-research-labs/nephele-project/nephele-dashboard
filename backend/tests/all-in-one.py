import requests
import json
# Base URL of the Flask application
BASE_URL = 'http://localhost:5000'

hdag_descriptor = {
    "description": "string",
    "hdaGraphIntent": "string",
    "id": "string",
    "services": [
        {
            "deploymentType": "auto",
            "intent": {
                "affinity": "string",
                "consumption": "LOW",
                "location": "FOLLOWUSER",
                "sla": "MMTC"
            },
            "ref": "string"
        }
    ]
}

# Registration endpoint
def register_user(username, password, email):
    url = f"{BASE_URL}/register"
    data = {
        'username': username,
        'password': password,
        'email': email
    }
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, data=json.dumps(data), headers=headers)
    print(response.json())

# Login endpoint
def login(username, password):
    url = f"{BASE_URL}/login"
    data = {
        'username': username,
        'password': password
    }
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, data=json.dumps(data), headers=headers)
    if response.status_code == 200:
        access_token = response.json().get('access_token')
        refresh_token = response.json().get('refresh_token')
        print (access_token)
        return access_token, refresh_token
    else:
        print("Login failed.")
        return None, None

# Create project endpoint
def create_project(project_name, access_token):
    url = f"{BASE_URL}/create_project"
    headers = {'Authorization': f'Bearer {access_token}'}
    data = {'project': project_name}
    response = requests.post(url, headers=headers, data=data)
    print(response.json())

# Fetch artifacts endpoint
def fetch_artifacts(access_token, project_name, artifact_type):
    print ("Testing fetch information")
    url = f"{BASE_URL}/{project_name}/fetch_artifacts"
    headers = {'Authorization': f'Bearer {access_token}'}
    data = {'artifact_type': artifact_type, 'project_name': project_name}
    response = requests.post(url, headers=headers, json=data)
    print(response.json())

# Fetch artifacts information endpoint
def fetch_artifacts_information(access_token, project_name, artifact_id, artifact_type):
    print ("Testing fetch_artifacts_information")
    url = f"{BASE_URL}/{project_name}/fetch_artifacts_information"
    headers = {'Authorization': f'Bearer {access_token}'}
    data = {'artifact_id' : artifact_id,'artifact_type': artifact_type, 'project_name': project_name}
    response = requests.post(url, headers=headers, json=data)
    print(response.json())

# Test the /clusters endpoint
def test_clusters_endpoint(access_token):
    print('Testing /clusters endpoint...')
    url = f"{BASE_URL}/clusters"
    headers = {'Authorization': f'Bearer {access_token}'}
    response = requests.get(url, headers=headers)
    print(f'Response Status Code: {response.status_code}')
    print('Response JSON:')
    print(response.json())

# Test the /devices endpoint
def test_devices_endpoint(access_token):
    print('Testing /devices endpoint...')
    url = f"{BASE_URL}/devices"
    headers = {'Authorization': f'Bearer {access_token}'}
    response = requests.get(url, headers=headers)
    print(f'Response Status Code: {response.status_code}')
    print('Response JSON:')
    print(response.json())

def test_refresh_token(refresh_token):
    url = f"{BASE_URL}/refresh_token"
    data = {'refresh_token': refresh_token}
    response = requests.post(url, data=data)
    if response.status_code == 200:
        access_token = response.json().get('access_token')
        print(f"Access token: {access_token}")
    else:
        print("Token refresh failed.")

def test_get_user_groups(access_token):
    url = f"{BASE_URL}/user_groups"
    headers = {'Authorization': f'Bearer {access_token}'}
    response = requests.post(url, headers=headers)
    print(response.json())


def publish_hdag(descriptor_json, project_name, access_token):
    url = f"{BASE_URL}/publish_hdag"
    headers = {
        'accept': 'text/plain',
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {access_token}'
    }
    hdag_json = {'project_name' : project_name,'hdag_descriptor': descriptor_json}

    response = requests.post(url, json=hdag_json, headers=headers)
    if response.status_code == 200:
        print("HDAG published successfully.")
        print("Response:")
        print(response.text)
    else:
        print(f"Error: {response.status_code} - {response.text}")

# Main function to test all endpoints
def test_endpoints():
    # Register a new user
    # register_user('testuser', 'testpassword', 'test@example.com')
    project_name = 'brussels-demo'
    artifact_type = 'HDAG' #'VO'
    artifact_id = 'brussels-demo' # 'image-compression-vo' #
    # Login as the registered user
    access_token, refresh_token = login('testuser', 'testpassword')
    # test_refresh_token(refresh_token)
    # test_get_user_groups(access_token)
    # create_project(project_name, access_token)

    test_devices_endpoint(access_token)
    test_clusters_endpoint(access_token)
    # fetch_artifacts(access_token, project_name, artifact_type)
    # fetch_artifacts_information (access_token, artifact_id, project_name, artifact_type)
    # publish_hdag(hdag_descriptor, project_name, access_token)
    print ("refresh token", refresh_token)
test_endpoints()