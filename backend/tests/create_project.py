import json
import requests 

# Flask login endpoint URL
login_url = 'http://localhost:5000/login'

# Request payload for login
login_data = {
    'username': 'example_user',
    'password': 'your_password'
}

# Make the login request
login_response = requests.post(login_url, json=login_data)

# Check if the login request was successful
if login_response.status_code == 200:
    # Extract the access token from the JSON response
    access_token = login_response.json().get('access_token')
    
    # URL of the Flask application for creating project
    create_project_url = 'http://localhost:5000/create_project'

    # Project name provided by the user
    project_name = "nephele_test" #input("Enter project name: ")

    # Payload for the POST request to create project
    create_project_payload = {
        "name": project_name
    }

    # Headers for the POST request
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {access_token}'  # Include access token in headers
    }

    # Make the POST request to create project
    create_project_response = requests.post(create_project_url, json=create_project_payload, headers=headers)
    print ("Status", create_project_response.status_code)
    # Check the response for creating project
    if create_project_response.status_code == 200:
        print("Project created successfully!")
        print("Response:", create_project_response.json())
    elif create_project_response.status_code == 401:
        print("User is not logged in. Please log in first.")
    elif create_project_response.status_code == 400:
        print("Bad request. Please provide a valid project name.")
    else:
        print("Failed to create project. Status code:", create_project_response.status_code)
        print("Response:", create_project_response.text)
else:
    print("Login failed")