import requests

# Flask login endpoint URL
login_url = 'http://localhost:5000/login'

# Request payload
data = {
    'username': 'example_user',
    'password': 'your_password',
    'email' : 'example@example.com'
}

# Make the request
response = requests.post(login_url, json=data)

# Check if the request was successful
if response.status_code == 200:
    # Extract the access token from the JSON response
    access_token = response.json().get('access_token')
    print("Access token:", access_token)
else:
    print("Login failed")