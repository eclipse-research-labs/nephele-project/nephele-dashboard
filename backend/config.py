import os
from dotenv import load_dotenv

# Get the path of the directory where the Flask app is located
app_dir = os.path.abspath(os.path.dirname(__file__))
SQLITE_DATABASE_PATH = os.path.join(app_dir, 'dashboard.db')

load_dotenv()

class Config:
    FLASK_USERNAME = os.getenv('FLASK_USERNAME')
    FLASK_PASSWORD = os.getenv('FLASK_PASSWORD')

    CLIENT_ID = os.getenv('CLIENT_ID')
    CLIENT_SECRET = os.getenv('CLIENT_SECRET')

    KEYCLOAK_URL = os.getenv('KEYCLOAK_URL')
    REALM_NAME = os.getenv('REALM_NAME')
    SECRET_KEY = os.getenv('SECRET_KEY', 'your_secret_key')
    FORCE_HTTPS = False

    HDAR_BASE_URL = os.getenv('HDAR_BASE_URL')
    SMO_BASE_URL = os.getenv('SMO_BASE_URL')

    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(SQLITE_DATABASE_PATH)


class ProdConfig(Config):
    FLASK_ENV = 'production'
    DEBUG = False

class DevConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True

configs = {
    'default': ProdConfig,
    'production': ProdConfig,
    'development': DevConfig,
}

dashboard_config = Config()
