"""DB models declaration."""

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from models.admin_settings import AdminSettings
from models.registered_iot_device import RegisteredIoTDevice
