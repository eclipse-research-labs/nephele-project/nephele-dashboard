"""IoT device table."""

from models import db


class RegisteredIoTDevice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    device_type = db.Column(db.String(100), nullable=False)
    ip = db.Column(db.String(100), nullable=False)
    thing_descriptor = db.Column(db.JSON, nullable=False)
    location = db.Column(db.String(100), nullable=False)

    def to_dict(self):
        """Returns a dictionary representation of the class."""

        return {
            'id': self.id,
            'device_type': self.device_type,
            'ip': self.ip,
            'thing_descriptor': self.thing_descriptor,
            'location': self.location
        }