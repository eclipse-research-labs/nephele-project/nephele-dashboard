from models import db

class AdminSettings(db.Model):
    __tablename__ = 'admin_settings'
    id = db.Column(db.Integer, primary_key=True)
    admin_dashboard_password = db.Column(db.String(255), nullable=False)
