import os
from models import db, RegisteredIoTDevice ,AdminSettings


def update_device(device_id, data):
    device = db.session.query(RegisteredIoTDevice).get(device_id)
    if not device:
        return False
    device.device_type = data.get('device_type', device.device_type)
    device.ip = data.get('ip', device.ip)
    device.thing_descriptor = data.get('thing_descriptor', device.thing_descriptor)
    device.location = data.get('location', device.location)
    db.session.commit()
    return True


# Function to add initial data to tables
def add_initial_data():
    # Add initial devices
    TD = {
        "title": "vo",
        "id": "urn:dev:wot:plenary:vo",
        "description": "Image detection VO.",
        "securityDefinitions": {
            "no_sc": {
                "scheme": "nosec"
            }
        },
        "security": "no_sc",
        "@context": [
            "https://www.w3.org/2022/wot/td/v1.1"
        ],
        "actions": {
            "detectImage": {
                "type": "boolean"
            }
        }
        # "title": "device1",
        # "id": "urn:dev:wot:plenary:device1",
        # "description": "'Lille Plenary Meeting Descriptor for Device 1 (with computing capabilities).'",
        # "securityDefinitions": {
        #     "basic_sec": {
        #         "scheme": "basic"
        #     }
        # },
        # "security": "basic_sec",
        # "@context": [
        #     "https://www.w3.org/2022/wot/td/v1.1"
        # ],
        # "properties": {
        #     "temperature": {
        #         "type": "integer"
        #     },
        #     "humidity": {
        #         "type": "integer"
        #     }
        # },
        # "actions": {
        #     "currentValues": {
        #         "description": "Returns the current values"
        #     }
        # }
    }
    device1 = RegisteredIoTDevice(
        device_type='Raspberry Pi',
        ip='147.102.13.100',
        thing_descriptor=TD,
        location='Netmode'
    )
    db.session.add_all([device1])

    existing_setting = AdminSettings.query.first()
    if not existing_setting:
        # Pull admin password from environment variable, fallback to a default
        admin_password = os.environ.get('ADMIN_DASHBOARD_PASSWORD', 'MySecretAdminPass')
        new_setting = AdminSettings(admin_dashboard_password=admin_password)
        db.session.add(new_setting)

    db.session.commit()
