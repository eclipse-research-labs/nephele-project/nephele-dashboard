from keycloak import KeycloakAdmin, KeycloakOpenID, KeycloakGetError, \
    KeycloakPostError, KeycloakPutError, KeycloakAuthenticationError

from config import dashboard_config

class KeycloakClient:
    def __init__(self):
        self.keycloak_admin = KeycloakAdmin(
            server_url=dashboard_config.KEYCLOAK_URL,
            username=dashboard_config.FLASK_USERNAME,
            password=dashboard_config.FLASK_PASSWORD,
            realm_name=dashboard_config.REALM_NAME,
            client_id=dashboard_config.CLIENT_ID,
            client_secret_key=dashboard_config.CLIENT_SECRET,
            verify=True
        )

        self.keycloak_openid = KeycloakOpenID(
            server_url=dashboard_config.KEYCLOAK_URL,
            client_id=dashboard_config.CLIENT_ID,
            realm_name=dashboard_config.REALM_NAME,
            client_secret_key=dashboard_config.CLIENT_SECRET
        )

        self.backend_access_token = self.keycloak_admin.token['access_token']


    def obtain_user_tokens(self, username, password):
        tokens = self.keycloak_openid.token(username, password)
        access_token = tokens['access_token']
        refresh_token = tokens['refresh_token']
        return access_token, refresh_token


    def validate_access_token(self, access_token_header):
        try:
            if not access_token_header or not access_token_header.startswith('Bearer '):
                raise Exception('Invalid access token')

            access_token = access_token_header.split(' ')[1]
            token_info = keycloak_client.keycloak_openid.userinfo(access_token)
            if not token_info:
                raise Exception('Invalid or expired access token')

        except KeycloakAuthenticationError:
            raise Exception('Authentication failed')

        return access_token, token_info


    def get_userinfo(self, access_token):
        # Get userinfo from Keycloak using the access token
        try:
            userinfo = keycloak_client.keycloak_openid.userinfo(access_token)
        except KeycloakAuthenticationError:
            raise Exception('Access token invalid')
        if not userinfo:
            raise Exception('Failed to get userinfo from Keycloak')

        return userinfo


    def retrieve_user_id(self, username):
        try:
            user_id = keycloak_client.keycloak_admin.get_user_id(username)
            if user_id is None:
                raise Exception('User does not exist')
        except KeycloakGetError:
            raise Exception('User not found')
        return user_id


    def add_user_to_group(self, user_id, group_id):
        try:
            keycloak_client.keycloak_admin.group_user_remove(user_id, group_id)
        except KeycloakPutError as e:
            print('User not in group, proceeding with adding')

        # Attempt to add user again
        try:
            keycloak_client.keycloak_admin.group_user_add(user_id, group_id)
        except KeycloakPutError as e:
            raise Exception(f'Failed to add user to group: {str(e)}')
        except Exception as e:
            raise Exception(f'An error occurred while adding the user to the group: {str(e)}')

        return {'success': True}


    def check_user_group(self, project_name):
        groups=[]
        try:
            groups = keycloak_client.keycloak_admin.get_groups(query={'name': project_name})
        except KeycloakGetError:
            raise Exception('Project group not found')
        for group in groups:
            if group['name'] == project_name:
                group_id = group['id']
                return group_id
        return None


    def check_user_groups(self, project_name):
        try:
            group_id = keycloak_client.keycloak_admin.get_groups(query={'name': project_name})
        except KeycloakGetError:
            raise Exception('Project group not found')

        return group_id


    def create_project_group(self, project_name):
        try:
            group_id = keycloak_client.keycloak_admin.create_group(payload={'name': project_name})
        except KeycloakPostError:
            group_id = keycloak_client.keycloak_admin.get_groups(query={'name': project_name})
            raise Exception('Failed to create project group. Group already exists')

        return group_id


keycloak_client = KeycloakClient()
