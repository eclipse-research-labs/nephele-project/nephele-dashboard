from functools import wraps

from flask import request, jsonify
from keycloak import KeycloakAuthenticationError
from werkzeug.exceptions import BadRequest

from utils.keycloak_client import keycloak_client


# List of routes that do not require authorization
PUBLIC_ROUTES = [
    '/publish_hdag',
    '/fetch_artifacts_information',
    '/fetch_artifacts',
    '/app_graphs',
    '/start_graph',
    '/stop_graph',
    '/refresh_token',
    '/clusters',
    '/devices',
    '/login',
    '/register',
    '/logout',
    '/create_project',
    '/user_groups'
]


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # Exclude certain routes from requiring authorization
        if request.path in PUBLIC_ROUTES:
            return f(*args, **kwargs)
        # Intercept requests and check group access
        access_token_header = request.headers.get('Authorization')
        if access_token_header and access_token_header.startswith('Bearer '):
            access_token = access_token_header.split(' ')[1]  # Extract the token part after 'Bearer '
            if not access_token:
                return jsonify({'error': 'Access token missing'}), 401
            # Fetch userinfo using access token
            try:
                keycloak_client.keycloak_openid.userinfo(access_token)
            except KeycloakAuthenticationError:
                return jsonify({'error': 'Access denied'}), 403
        else:
            return jsonify({'error': 'Access denied'}), 403
        return f(*args, **kwargs)
    return decorated_function


# Define a decorator to add spec to route
def doc(spec):
    def wrapper(fn):
        @wraps(fn)
        def decorated(*args, **kwargs):
            return fn(*args, **kwargs)
        # Add spec to route
        spec.path(view=decorated)
        return decorated
    return wrapper


def is_user_in_group(user_id, group_name):
    # Check if the user belongs to the specified group
    user_groups = keycloak_client.keycloak_admin.get_user_groups(user_id=user_id)
    return any(group['name'] == group_name for group in user_groups)


def check_group_access():
    if not keycloak_client.backend_access_token:
        return jsonify({'error': 'Failed to obtain backend access token'}), 500

    # Exclude certain routes from requiring authorization
    if request.path in PUBLIC_ROUTES:
        return

    try:
        data=request.json
        group_name = data.get('project_name')
        # print ("group", group_name)
    except BadRequest:
        print ("No project name in the request")
    if not group_name:
        # Intercept requests and check group access
        path_parts = request.path.split('/')
        group_name = path_parts[1] if len(path_parts) > 1 else None  # Extract the group name from the URL
        # print(group_name)

    # Intercept requests and check group access
    access_token_header = request.headers.get('Authorization')
    if access_token_header and access_token_header.startswith('Bearer '):
        access_token = access_token_header.split(' ')[1]  # Extract the token part after 'Bearer '
        if not access_token:
            return jsonify({'error': 'Access token missing'}), 401
        # Fetch userinfo using access token
        try:
            userinfo = keycloak_client.keycloak_openid.userinfo(access_token)
        except KeycloakAuthenticationError:
            return jsonify({'error': 'Access denied'}), 403
        username = userinfo.get('preferred_username')
        if not username:
            return jsonify({'error': 'Username not found in userinfo'}), 401
        user_id = keycloak_client.keycloak_admin.get_user_id(username=username)
        if not is_user_in_group(user_id, group_name):
            return jsonify({'error': 'Access denied'}), 403
    else:
        return jsonify({'error': 'Access denied'}), 403
