import base64
import os

import requests
from flasgger import swag_from
from flask import Blueprint, request, jsonify, current_app


artifact = Blueprint('artifact', __name__)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@artifact.route('/fetch_artifacts', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/fetch_artifacts.yml'))
def fetch_artifacts():
    hdar_base_url = current_app.config["HDAR_BASE_URL"]
    url = f'{hdar_base_url}/catalogue'

    data = request.json
    if not data:
        return jsonify({'error': 'No JSON data provided'}), 400

    # Extract the artifact type and project name from the JSON data
    # artiface type can be one of the following [VO,CVO,HDAG,APP]
    artifact_type = data.get('artifact_type')
    project_name = data.get('project_name')

    # Construct parameters based on the provided artifact type
    params = {'artifactType': artifact_type}
    headers = {'accept': 'application/json'}
    # Perform the initial request to the first endpoint
    response = requests.get(url, params=params, headers=headers)

    if response.status_code != 202:
        return jsonify({'error': f"Failed to fetch data from {url}"}), 500

    # Filter the responses based on the project_name
    filtered_responses = [r for r in response.json() if project_name in r]
    # Extract artifact IDs from the filtered responses
    artifact_ids = [r.split('/')[-1] for r in filtered_responses]
    # Fetch information for each artifact ID
    artifact_info = {}
    for artifact_id in artifact_ids:
        artifact_url = f'{hdar_base_url}/catalogue/{artifact_id}/config'
        params = {'project': {project_name}}
        headers = {'accept': 'application/json'}

        artifact_response = requests.get(artifact_url, params=params, headers=headers)

        if artifact_response.status_code == 200:
            artifact_info[artifact_id] = artifact_response.json()
            print ("id", artifact_id)
        else:
            artifact_info[artifact_id] = {'error': f"Failed to fetch data from {artifact_url}"}
    return jsonify(artifact_info)


@artifact.route('/fetch_artifacts_information', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/fetch_artifacts_information.yml'))
def fetch_artifacts_information():
    hdar_base_url = current_app.config["HDAR_BASE_URL"]

    data = request.json
    artifact_id = data.get('artifact_id')
    project_name = data.get('project_name')

    # Hit the endpoint with the provided artifact ID and project name
    url = f'{hdar_base_url}/catalogue/{artifact_id}/content'
    params = {'project': project_name}
    headers = {'accept': 'application/json'}
    response = requests.get(url, params=params, headers=headers)
    if response.status_code == 200:
        decoded_content = []
        for artifact in response.json():
            content_bytes = base64.b64decode(artifact['content'])
            decoded_content.append({'fileName': artifact['fileName'], 'content': content_bytes.decode('utf-8')})
        return jsonify(decoded_content)
    else:
        return jsonify({'error': f"Failed to fetch data from {url}"}), response.status_code
