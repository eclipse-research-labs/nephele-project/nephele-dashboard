import os

from flasgger import swag_from
from flask import Blueprint, jsonify

from models import db, RegisteredIoTDevice
from utils.utils import login_required


device = Blueprint('device', __name__)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@device.route('/devices', methods=['GET'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/devices.yml'))
@login_required
def get_devices():
    devices = db.session.query(RegisteredIoTDevice).all()
    device_list = [device.to_dict() for device in devices]
    return jsonify({'devices': device_list}), 200