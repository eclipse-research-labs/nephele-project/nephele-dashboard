import os

from flasgger import swag_from
from flask import Blueprint, request, jsonify
from keycloak import KeycloakPostError, KeycloakPutError

from models import AdminSettings
from utils.utils import login_required
from utils.keycloak_client import keycloak_client


user = Blueprint('user', __name__)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@user.route('/user_groups', methods=['POST'])
@login_required
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/user_groups.yml'))
def get_user_groups():
    # Extract access token from request headers
    access_token_header = request.headers.get('Authorization')

    # Validate access token and get token_info
    try:
        access_token, token_info = keycloak_client.validate_access_token(access_token_header)
        print("access token", access_token)
    except Exception as e:
        print("error", e)
        return jsonify({"error": str(e)}), 401

    # Get userinfo from Keycloak using the access token
    try:
        userinfo = keycloak_client.get_userinfo(access_token)
    except Exception as e:
        return jsonify({"error": str(e)}), 401

    # Extract username from userinfo
    username = userinfo.get('preferred_username')
    if not username:
        return jsonify({'error': 'Username not found in userinfo'}), 401

    #Fetch user_id
    try:
        user_id =  keycloak_client.retrieve_user_id(username)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    if not user_id:
        return jsonify({'error': 'Failed to retrieve user ID'}), 404

    # Get groups that the user belongs to using KeycloakAdmin
    user_groups = keycloak_client.keycloak_admin.get_user_groups(user_id=user_id)

    # Everything went ok return groups for specific username
    return jsonify({'username': username, 'groups': user_groups})


# Route for creating a new project
@user.route('/create_project', methods=['POST'])
@login_required
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/create_project.yml'))
def create_project():
    # Ensure backend has a valid access token
    if not keycloak_client.backend_access_token:
        return jsonify({'error': 'Failed to obtain backend access token'}), 500

    # Extract project data from the request
    project_name = request.form['project']

    # Create group
    try:
        group_id = keycloak_client.create_project_group(project_name)
    except Exception as e:
        return jsonify({"error": str(e)}), 409

    # Add the current user to the newly created group
    # Intercept requests and check group access
    access_token_header = request.headers.get('Authorization')
    # Validate access token and get token_info
    try:
        access_token, token_info = keycloak_client.validate_access_token(access_token_header)
        print("access token", access_token)
    except Exception as e:
        print("error", e)
        return jsonify({"error": str(e)}), 401

    # Get userinfo from Keycloak using the access token
    try:
        userinfo = keycloak_client.get_userinfo(access_token)
    except Exception as e:
        return jsonify({"error": str(e)}), 401

    # Extract username from userinfo
    username = userinfo.get('preferred_username')
    if not username:
        return jsonify({'error': 'Username not found in userinfo'}), 401

    #Retrieve user_id with username
    try:
        user_id = keycloak_client.retrieve_user_id(username)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    if not user_id:
        return jsonify({'error': 'Failed to retrieve user ID'}), 404

    #Add user to the project group
    try:
        keycloak_client.add_user_to_group(user_id, group_id)
    except Exception as e:
        return jsonify({"error": str(e)}), 409

    return jsonify({'message': 'Project created successfully and User is Assigned to Group'})


@user.route('/refresh_token', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/refresh_token.yml'))
def refresh_token():
    refresh_token = request.form['refresh_token']
    try:
        tokens = keycloak_client.keycloak_openid.refresh_token(refresh_token)
    except KeycloakPostError:
        return jsonify({'error': 'You have to login again'}), 403
    access_token = tokens['access_token']
    if access_token:
        return jsonify({'access_token': access_token})
    else:
        return jsonify({'error': 'Token refresh failed'}), 401


# Registration route
@user.route('/register', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/register.yml'))
def register():
    # Get registration data from the request
    data = request.json
    admin_dashboard_password = data.get('admin_dashboard_password', '')
    if not admin_dashboard_password:
        return jsonify({'error': 'Missing admin_dashboard_password'}), 400

    settings = AdminSettings.query.first()
    if not settings:
        return jsonify({'error': 'No admin dashboard password configured in DB'}), 500

    if admin_dashboard_password != settings.admin_dashboard_password:
        return jsonify({'error': 'Invalid admin dashboard password'}), 401

    # Extract username, password, and email from the JSON data
    username = data.get('username')
    password = data.get('password')
    email = data.get('email')
    payload = {
        'username': username,
        'email': email,
        'enabled': True,  # Enable the user automatically
        'credentials': [{
            'type': 'password',
            'value': password,
            'temporary': False
        }]
    }

    #TODO be sure that the admin has refreshed token!!
    # Register user

    # Add any additional fields required by Keycloak for user registration
    try:
        user_id = keycloak_client.keycloak_admin.create_user(payload=payload)
    except KeycloakPostError:
        return jsonify({'error': 'User exists'}), 409

    # Assign role to user
    # role_name = 'developer'
    # role_id = keycloak_client.keycloak_admin.get_realm_role(role_name)
    # if role_id:
    #     keycloak_client.keycloak_admin.assign_realm_roles(user_id=user_id, roles= role_id)
    #     print(f"Role '{role_name}' assigned to user '{username}' successfully.")
    # else:
    #     print(f"Role '{role_name}' not found.")

    if user_id:
        return jsonify({'message': 'User registered successfully'}), 200
    else:
        return jsonify({'error': 'Failed to register user'}), 500


# Login route
@user.route('/login', methods=['POST'])
@swag_from('swagger_docs/login.yml')
def login():
    data = request.json
    username = data.get('username')
    password = data.get('password')
    access_token, refresh_token = keycloak_client.obtain_user_tokens(username, password)
    if access_token and refresh_token:
        access_token, refresh_token = keycloak_client.obtain_user_tokens(username, password)
        # Authentication successful, return tokens as JSON response
        return jsonify({'access_token': access_token, 'refresh_token': refresh_token})
    else:
        return jsonify({'error': 'Login failed'}), 401


# Change Password route
@user.route('/change_password', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/change_password.yml'))
def change_password():
    data = request.json
    username = data.get('username')
    current_password = data.get('password')
    new_password = data.get('new_password')
    if not username or not current_password or not new_password:
        return jsonify({"error": "All fields are required"}), 400

    #Authenticate the user
    try:
        token = keycloak_client.keycloak_openid.token(username, current_password)
    except Exception as e:
        return jsonify({"error": "Authentication failed", "details": str(e)}), 401

    #Retrieve user_id with username
    try:
        user_id = keycloak_client.retrieve_user_id(username)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    #Change the password
    try:
        keycloak_client.keycloak_admin.set_user_password(
            user_id=user_id,
            password=new_password,
            temporary=False
        )
    except Exception as e:
        return jsonify({"error": "Failed to change password", "details": str(e)}), 500

    return jsonify({"message": "Password changed successfully"}), 200


#Delete account route
@user.route('/delete_account', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/delete_account.yml'))
def delete_account():
    data = request.json
    username = data.get('username')
    password = data.get('password')
    if not username or not password:
        return jsonify({"error": "All fields are required"}), 400

    #Authenticate the user
    try:
        token = keycloak_client.keycloak_openid.token(username, password)
    except Exception as e:
        return jsonify({"error": "Authentication failed", "details": str(e)}), 401

    #Retrieve user_id with username
    try:
        user_id = keycloak_client.retrieve_user_id(username)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    #Delete account
    try:
        keycloak_client.keycloak_admin.delete_user(user_id)
    except Exception as e:
        return jsonify({"error": "Failed to delete account"}), 500

    return jsonify({"message": "Account deleted successfully"}), 200


#Delete project route
@user.route('/delete_project', methods=['POST'])
@login_required
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/delete_project.yml'))
def delete_project():
    data = request.json
    admin_password = data.get('admin_dashboard_password')
    settings = AdminSettings.query.first()
    if not admin_password or not settings or admin_password != settings.admin_dashboard_password:
        return jsonify({"error": "Invalid or missing admin dashboard password"}), 401

    project_name = data.get('project')
    access_token_header = request.headers.get('Authorization')

    #Validate access_token and token_info
    try:
        access_token, token_info = keycloak_client.validate_access_token(access_token_header)
        print("access token", access_token)
    except Exception as e:
        print("error", e)
        return jsonify({"error": str(e)}), 401

    #Check if project group exists
    try:
        group_id = keycloak_client.check_user_group(project_name)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    #Delete project group
    try:
        keycloak_client.keycloak_admin.delete_group(group_id)
    except Exception as e:
        return jsonify({"error": "Failed to delete project group", "details": str(e)}), 500

    return jsonify({"message": "Project group deleted successfully"}), 200


#Add user to project route
@user.route('/add_user_to_project', methods=['POST'])
@login_required
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/add_user_to_project.yml'))
def add_user_to_project():
    data = request.json
    admin_password = data.get('admin_dashboard_password')
    settings = AdminSettings.query.first()
    if (not admin_password) or (not settings) or (admin_password != settings.admin_dashboard_password):
        return jsonify({"error": "Invalid or missing admin dashboard password"}), 401

    project_name = data.get('project')
    username = data.get('username')
    access_token_header = request.headers.get('Authorization')

    #Validate access_token and token_info
    try:
        access_token, token_info = keycloak_client.validate_access_token(access_token_header)
        print("access token", access_token)
    except Exception as e:
        print("error", e)
        return jsonify({"error": str(e)}), 401
    #Check if project group exists
    try:
        group_id = keycloak_client.check_user_group(project_name)
    except Exception as e:
        return jsonify({"error": str(e)}), 404
    #Retrieve user_id with username
    try:
        user_id = keycloak_client.retrieve_user_id(username)
    except Exception as e:
        return jsonify({"error": str(e)}), 404
    #Add user to the project group
    try:
        keycloak_client.add_user_to_group(user_id, group_id)
    except Exception as e:
        return jsonify({"error": str(e)}), 409

    return jsonify({'message': 'User is successfully added to the project group'}), 200


#Remove user from project route
@user.route('/remove_user_from_project', methods=['POST'])
@login_required
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/remove_user_from_project.yml'))
def remove_user_from_project():
    data = request.json
    admin_password = data.get('admin_dashboard_password')
    settings = AdminSettings.query.first()
    if (not admin_password) or (not settings) or (admin_password != settings.admin_dashboard_password):
        return jsonify({"error": "Invalid or missing admin dashboard password"}), 401

    project_name = data.get('project')
    username = data.get('username')
    access_token_header = request.headers.get('Authorization')

    #Validate access_token and token_info
    try:
        access_token, token_info = keycloak_client.validate_access_token(access_token_header)
    except Exception as e:
        print("error", e)
        return jsonify({"error": str(e)}), 401

    #Check if project group exists
    try:
        group_id = keycloak_client.check_user_group(project_name)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    #Retrieve user_id with username
    try:
        user_id = keycloak_client.retrieve_user_id(username)
    except Exception as e:
        return jsonify({"error": str(e)}), 404

    #Remove user from group
    try:
        keycloak_client.keycloak_admin.group_user_remove(user_id, group_id)
    except KeycloakPutError:
        return jsonify({"message": "User already belongs to the group"}), 409
    except Exception as e:
        return jsonify({"message": "An error occurred while removing the user from the group", "details": str(e)}), 500


    return jsonify({'message': 'User is successfully removed from the project group'}), 200


# Route for creating a new project without assigning users to it
@user.route('/create_project_no_assigned_users', methods=['POST'])
@login_required
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/create_project_no_assigned_users.yml'))
def create_project_no_assigned_users():
    data = request.json
    admin_password = data.get('admin_dashboard_password')
    settings = AdminSettings.query.first()
    if not admin_password or not settings or admin_password != settings.admin_dashboard_password:
        return jsonify({"error": "Invalid or missing admin dashboard password"}), 401

    # Ensure backend has a valid access token
    if not keycloak_client.backend_access_token:
        return jsonify({'error': 'Failed to obtain backend access token'}), 500

    # Extract project data from the request
    project_name = data.get('project')

    # Create group
    try:
        group_id = keycloak_client.create_project_group(project_name)
    except Exception as e:
        return jsonify({"error": str(e)}), 409

    return jsonify({'message': 'Project created successfully.'}), 200
