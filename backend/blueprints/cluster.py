import os

import requests
from flasgger import swag_from
from flask import Blueprint, current_app, jsonify

from utils.utils import login_required


cluster = Blueprint('cluster', __name__)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@cluster.route('/clusters', methods=['GET'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/clusters.yml'))
@login_required
def get_clusters():
    smo_base_url = current_app.config["SMO_BASE_URL"]

    url = f'{smo_base_url}/clusters'
    response = requests.get(url)
    print(response)

    return jsonify({'clusters': response.json()}), 200
