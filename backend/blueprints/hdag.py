import os

import requests

from flasgger import swag_from
from flask import Blueprint, request, jsonify, current_app


hdag = Blueprint('hdag', __name__)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@hdag.route('/publish_hdag', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/publish_hdag.yml'))
def publish_hdag():
    hdar_base_url = current_app.config["HDAR_BASE_URL"]
    smo_base_url = current_app.config["SMO_BASE_URL"]

    data = request.json
    project_name = data.get('project_name')
    if not project_name:
        return jsonify({'error': 'Project name is missing in the request parameters'}), 400
    descriptor_json = data.get('hdag_descriptor')  # Assuming the request body contains JSON data
    url = f'{hdar_base_url}/hdag?project={project_name}'
    headers = {'accept': 'text/plain', 'Content-Type': 'application/json'}
    # response = requests.post(url, json=descriptor_json, headers=headers)
    if True: #response.status_code == 200:
        url = f'{smo_base_url}/project/{project_name}/graphs'
        headers = {'Content-Type': 'application/json'}
        response = requests.post(url, headers=headers, json=descriptor_json)
        return response.text #TODO propably trigger SMO! Send also the project in which is the HDAG
    else:
        return f"Error: {response.status_code} - {response.text}", response.status_code

#TODO Test and finalize it is not working
@hdag.route('/update_hdag', methods=['POST'])
# @swag_from('swagger_docs/update_hdag.yml')
def update_hdag():
    hdar_base_url = current_app.config["HDAR_BASE_URL"]

    data = request.json
    project_name = data.get('project_name')
    if not project_name:
        return jsonify({'error': 'Project name is missing in the request parameters'}), 400
    descriptor_json = data.get('hdag_descriptor')  # Assuming the request body contains JSON data
    url = f'{hdar_base_url}/hdag?project={project_name}'
    headers = {'accept': 'text/plain', 'Content-Type': 'application/json'}
    response = requests.post(url, json=descriptor_json, headers=headers)
    print (response.status_code)
    if response.status_code == 200:
        return response.text #TODO propably trigger SMO! Send also the project in which is the HDAG
    else:
        return f"Error: {response.status_code} - {response.text}", response.status_code

@hdag.route('/app_graphs', methods=['POST'])
@swag_from(os.path.join(BASE_DIR, 'swagger_docs/app_graphs.yml'))
def app_graphs():
    smo_base_url = current_app.config["SMO_BASE_URL"]

    data = request.json
    project_name = data.get('project_name')
    url = f'{smo_base_url}/project/{project_name}/graphs'
    response = requests.get(url)
    data = response.json()
    return jsonify(data)

@hdag.route('/stop_graph', methods=['POST'])
def stop_graph():
    smo_base_url = current_app.config["SMO_BASE_URL"]

    data = request.json
    graph_name = data.get("graph_name")
    print (graph_name)
    url = f'{smo_base_url}/graphs/{graph_name}/stop'
    response = requests.get(url)
    print ("stop")
    return jsonify({'Graph': "is Stopping"}), 200

@hdag.route('/start_graph', methods=['POST'])
def start_graph():
    smo_base_url = current_app.config["SMO_BASE_URL"]

    data = request.json
    graph_name = data.get("graph_name")
    print (graph_name)
    url = f'{smo_base_url}/graphs/{graph_name}/start'
    response = requests.get(url)
    print ("start")
    return jsonify({'Graph': "is Starting"}), 200
