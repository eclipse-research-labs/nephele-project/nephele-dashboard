"""Main Flask app entrypoint."""

import os

from flasgger import Swagger
from flask import Flask

from config import configs
from models import db
from blueprints.artifact import artifact
from blueprints.cluster import cluster
from blueprints.device import device
from blueprints.hdag import hdag
from blueprints.user import user
from models.utils import add_initial_data

env = os.environ.get('FLASK_ENV', 'production')


def create_app(app_name='dashboard'):
    """
    Function that returns a configured Flask app.
    """

    ROOT_PATH = os.path.dirname(__file__)
    app = Flask(app_name, root_path=ROOT_PATH)

    app.config['SWAGGER'] = {
        'title': 'Nephele Backend API',
        'uiversion': 3,
        'specs_route': '/backend-api/',
        'basePath': '/web',
        'specs': [
            {
                'endpoint': 'nephele-spec',
                'route': '/nephele-spec.json',
                'rule_filter': lambda rule: True,
                'model_filter': lambda tag: True,
            }
        ],
        'ui_params': {
            'apisSorter': 'alpha',
            'operationsSorter': 'alpha',
            'tagsSorter': 'alpha'
        },
        'ui_params_text': (
            '{\n'
            '    "operationsSorter": (a, b) => {\n'
            '        var order = { "get": "0", "post": "1", "put": "2", "delete": "3" };\n'
            '        return order[a.get("method")].localeCompare(order[b.get("method")]);\n'
            '    }\n'
            '}'
        )
    }

    Swagger(app=app)

    # Load other configurations as needed
    app.config.from_object(configs[env])

    app.register_blueprint(artifact)
    app.register_blueprint(cluster)
    app.register_blueprint(device)
    app.register_blueprint(hdag)
    app.register_blueprint(user)


    db.init_app(app)
    with app.app_context():
        db.drop_all()
        db.create_all()
        add_initial_data()

    return app


if __name__ == '__main__':
    app = create_app()
    app.run()
